﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Interop;

namespace TicTacToe
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private byte[,] matrix; //матрица 3х3 для хранения результатов ходов. X = 1; O = 2;
        private bool switcher; //переключатель хода true - Х, false - О
        private bool isEnd; //определяет кончилась ли игра

        public MainWindow()
        {
            InitializeComponent();
            StartNewGame();
        }
        
        private void StartNewGame()
        {
            //Создание нового раунда игры

            //новая игра - ещё не закончена
            isEnd = false;

            //Обнуление игровой матрицы
            matrix = new byte[3,3]
            {
                {0,0,0},
                {0,0,0},
                {0,0,0}
            };

            //Random выбор первого хода
            switcher = new Random().Next(0,100) < 50 ? true : false;
            switch (switcher)
            {
                case true:
                    NextStepText.Text = @"Ходит 'X'";
                    break;
                case false:
                    NextStepText.Text = @"Ходит 'O'";
                    break;
            }

            //Убираем X и O с формы
            Rect00.Fill = new SolidColorBrush(Colors.White);
            Rect01.Fill = new SolidColorBrush(Colors.White);
            Rect02.Fill = new SolidColorBrush(Colors.White);
            Rect10.Fill = new SolidColorBrush(Colors.White);
            Rect11.Fill = new SolidColorBrush(Colors.White);
            Rect12.Fill = new SolidColorBrush(Colors.White);
            Rect20.Fill = new SolidColorBrush(Colors.White);
            Rect21.Fill = new SolidColorBrush(Colors.White);
            Rect22.Fill = new SolidColorBrush(Colors.White);
        }

        private void Rect00_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if(!isEnd) OneStep(0, 0);
        }

        private void Rect01_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!isEnd) OneStep(0, 1);
        }

        private void Rect02_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!isEnd) OneStep(0, 2);
        }

        private void Rect10_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!isEnd) OneStep(1, 0);
        }

        private void Rect11_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!isEnd) OneStep(1, 1);
        }

        private void Rect12_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!isEnd) OneStep(1, 2);
        }

        private void Rect20_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!isEnd) OneStep(2, 0);
        }

        private void Rect21_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!isEnd) OneStep(2, 1);
        }

        private void Rect22_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!isEnd) OneStep(2, 2);
        }

        private void NewMenu_Click(object sender, RoutedEventArgs e)
        {
            StartNewGame();
        }

        private void ExitMenu_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        //что происходит за один ход
        private void OneStep(byte x, byte y)
        {
            if (matrix[x, y] != 0) return;
            string xy = x + y.ToString();
            
            ImageBrush imgBrush = new ImageBrush();
            if (switcher)
            {
                matrix[x, y] = 1;
                imgBrush.ImageSource = new BitmapImage(new Uri(@"Pics/X.png", UriKind.Relative));
            }
            else
            {
                matrix[x, y] = 2;
                imgBrush.ImageSource = new BitmapImage(new Uri(@"Pics/O.png", UriKind.Relative));
            }
            
            switch (xy)
            {
                case "00":
                    Rect00.Fill = imgBrush;
                    break;
                case "01":
                    Rect01.Fill = imgBrush;
                    break;
                case "02":
                    Rect02.Fill = imgBrush;
                    break;
                case "10":
                    Rect10.Fill = imgBrush;
                    break;
                case "11":
                    Rect11.Fill = imgBrush;
                    break;
                case "12":
                    Rect12.Fill = imgBrush;
                    break;
                case "20":
                    Rect20.Fill = imgBrush;
                    break;
                case "21":
                    Rect21.Fill = imgBrush;
                    break;
                case "22":
                    Rect22.Fill = imgBrush;
                    break;
            }

            IsGgameOver();

            if(!isEnd) ChangeSwitcher();
        }

        private void IsGgameOver()
        {
            byte winner = 0;

            //проверяем матрицу
            for (int i = 0; i < 3; i++)
            {
                if (matrix[i, 0] == matrix[i, 1] && matrix[i, 1] == matrix[i, 2] && matrix[i, 0] != 0)
                {
                    winner = matrix[i, 0];
                    isEnd = true;
                    goto winnerLabel;
                }
                else if (matrix[0, i] == matrix[1, i] && matrix[1, i] == matrix[2, i] && matrix[0, i] != 0)
                {
                    winner = matrix[0, i];
                    isEnd = true;
                    goto winnerLabel;
                }
            }
            if (matrix[0, 0] == matrix[1, 1] && matrix[1, 1] == matrix[2, 2] && matrix[0, 0] != 0)
            {
                winner = matrix[0, 0];
                isEnd = true;
                goto winnerLabel;
            }
            else if (matrix[2, 0] == matrix[1, 1] && matrix[2, 0] == matrix[0, 2] && matrix[2, 0] != 0)
            {
                winner = matrix[2, 0];
                isEnd = true;
                goto winnerLabel;
            }

            winnerLabel:
            switch (winner)
            {
                case 0:
                    break;
                case 1:
                    MessageBox.Show("Победил X", "Игра закончена", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK, MessageBoxOptions.RightAlign);
                    NextStepText.Text = "Победил X";
                    return;
                case 2:
                    MessageBox.Show("Победил O", "Игра закончена", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK, MessageBoxOptions.RightAlign);
                    NextStepText.Text = "Победил O";
                    return;
                default:
                    return;
            }

            foreach (byte element in matrix)
            {
                if(element == 0) return;
            }

            MessageBox.Show("Победила дружба!", "Игра закончена", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK, MessageBoxOptions.RightAlign);
            NextStepText.Text = "Победила дружба!";
            isEnd = true;
        }

        private void ChangeSwitcher()
        {
            switcher = !switcher;

            switch (switcher)
            {
                case true:
                    NextStepText.Text = @"Ходит 'X'";
                    break;
                case false:
                    NextStepText.Text = @"Ходит 'O'";
                    break;
            }
        }


    }
}
